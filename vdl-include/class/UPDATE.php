<?php
require_once 'UFILE.php';
require_once 'EVENT.php';
require_once 'PLACE.php';
require_once 'TOKEN.php';


/**
 * class UPDATE
 * 
 */
class UPDATE extends CORE_MAIN
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/
   
  /**
   * 
   * @access private
   */
   	private $_id_msg;
  /**
   * 
   * @access private
   */
	private $_id_user;
  /**
   * 
   * @access private
   */
	private $_id_group;
  /**
   * 
   * @access public
   */
	public $_date_published;
  /**
   * 
   * @access public
   */
	public $_text;
  /**
   * 
   * @access public
   */
	public $_file;
  /**
   * 
   * @access public
   */
	public $_event;
  /**
   * 
   * @access public
   */
	public $_place;


	/*
	 * 
	 * 
	 *
	public function __construct($id_msg,$id_user,$id_group,$date_published,$text,$file=null,$event=null,$place=null){
		$_id_msg = $id_msg;
		$_id_user = $id_user;
		$_id_group = $id_group;
		$_date_published = $date_published;
		$_file = $file;
		$_event = $event;
		$_place = $place;
	}*/
	
	/**
	* 
	*
	* @param mixed _id_user 

	* @return array
	* @access public
	*/
	public function getupdates( $user, $num ) {
	  	$connection = parent::connect();
		$query = ("SELECT vdl_post.id_user, vdl_post.text, vdl_post.date_published, vdl_post.lat, vdl_post.lon
					FROM `vdl_post`
					WHERE id_user LIKE '$user'
					ORDER BY vdl_post.date_published DESC LIMIT $num");
					
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				array_push($arresult,$row);
			}
			return $arresult;
		}
	} // end of member function getupdates

	/**
	* 
	*
	* @param mixed _id_user 

	* @return array
	* @access public
	*/
	public function getwallupdates($user) {
	  	$connection = parent::connect();
		$query = "SELECT a.id_user, a.id, nick, b.image,email, a.date_published, a.text, a.lat, a.lon
					FROM vdl_post a
					JOIN vdl_user b ON b.id = id_user
					WHERE b.id
					IN ( SELECT DISTINCT a.id
						 FROM vdl_user a
						 INNER JOIN vdl_friend_of b
						 WHERE (a.id = b.user1 OR a.id = b.user2)
						 AND ( b.user1 ='".$user."' OR b.user2 ='".$user."')
						 AND ( b.status != 0)
					)
					ORDER BY  a.date_published DESC 
					LIMIT 0 , 15";
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				array_push($arresult,$row);
			}
			return $arresult;
		}
	} // end of member function getupdates
	
	
	/**
	* Devuelve las últimas $num actualizaciones del usuario reconocido por su $token
	*
	* @param mixed _id_user 

	* @return array
	* @access public
	*/
	public function getLastUpdates($token, $num){
		//Recuperamos el id del usuario que nos envía el token
		$TOKEN = new TOKEN();
		$id = $TOKEN->getEmailfromToken($token);
		
		//Recuperamos las ultimas actualizaciones del usuario
		return $this->getupdates($id, $num);
	}
	
	
	private function addUpdate($id, $text, $lat, $long){
		 //Creamos la conexion
		 $connection = parent::connect();
		 

	 	$query = ("INSERT INTO `vdl_post`(`id_user`, `id_group`,`date_published`,`text`,`lat`, `lon`)
			   VALUES('$id', 'Vidali', NOW(), '$text', '$lat', '$long' )");

		$result = $connection->query($query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $connection->error;
			die($message);
			return false;
		}
		else{
			return true;
		}
	}

	
	public function addUserUpdate($token, $text, $lat, $long){
		//Recuperamos el id del usuario que nos envía el token
		$TOKEN = new TOKEN();
		$id = $TOKEN->getEmailfromToken($token);
		$SEC = new CORE_SECURITY();
		$ACT = new CORE_ACTIONS();
		$text = $SEC->clear_text($text);
		$text = nl2br($text);
		$text = $ACT->meta_text($text);
		//Insertamos la ultima actualización del usuario
		return $this->addUpdate($id, $text, $lat, $long);
	}


	private function deleteUpdate($post){
		 //Creamos la conexion
		 $connection = parent::connect();
		 

	 	$query = ("DELETE FROM `vdl_post` 
	 				WHERE id LIKE '$post'");

		$result = $connection->query($query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $connection->error;
			die($message);
			return false;
		}
		else{
			return true;
		}
	}

	public function deleteUserUpdate($post){
		//Eliminamos la actualización del usuario
		return $this->deleteUpdate($post);
	}


} // end of UPDATE
?>

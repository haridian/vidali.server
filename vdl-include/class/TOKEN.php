 <?php

/**
 * class TOKEN
 * 
 */
class TOKEN extends CORE_MAIN{
  
  
	private function inserToken($token, $id, $ip, $cierre){
		 //Creamos la conexion
		 $connection = parent::connect();
		 
		 if ($cierre != "true"){
		 	$query = ("INSERT INTO `vdl_token`(`user_id`,`token`,`ip`,`expiration_time`)
				   VALUES('$id','$token','$ip', NOW() + INTERVAL 1 HOUR)");
		 }else{
			 $query = ("INSERT INTO `vdl_token`(`user_id`,`token`,`ip`,`expiration_time`)
				   VALUES('$id','$token','$ip', NULL)");
		 }
		$result = $connection->query($query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $connection->error;
			die($message);
			return false;
		}
		else{
			return true;
		}
	}


	private function updateToken($token, $id){
		$connection = parent::connect();
		 
		 $query = ("UPDATE `vdl_token` 
		 			SET `expiration_time`= NOW() + INTERVAL 1 HOUR 
		 			WHERE `token`LIKE '$token' AND `user_id` LIKE '$id'");
				   
		$result = $connection->query($query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			return true;
		}
	}
	
	
	private function deleteToken($token, $id){
		$connection = parent::connect();
		 
		 $query = ("DELETE FROM `vdl_token` 
		 			WHERE `token`LIKE '$token' AND `user_id` LIKE '$id'");
				   
		$result = $connection->query($query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			return true;
		}
	}
	
	public function getEmailfromToken($token){
		$connection = parent::connect();
		 
		 $query = ("SELECT vdl_token.user_id
		 			FROM `vdl_token` 
		 			WHERE `token` LIKE '$token'");
				   
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				array_push($arresult,$row[0]);
			}
			return $arresult[0];
		}
	}
	  
 
	public function gen_token($_USER,$ip,$cierre,$id){
		//Generamos numero aleatorio
		$randNum = mt_rand();
		
		//Generamos el token
		$seed = $randNum . $ip . $_USER;
		$token = sha1(md5(trim($seed)));
		
		//Guardamos el token
		$this->inserToken($token, $id, $ip, $cierre);
		
		//Devolvemos el token
		return $token;
		
	}// end of member function gen_token
	
	
} // end of TOKEN

?>

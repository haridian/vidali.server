<?php
require_once 'CORE_MAIN.php';
require_once 'TOKEN.php';


/**
 * class GROUP
 * 
 */
class GROUP extends CORE_MAIN
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/


  /**
   * 
   *
   * @return void
   * @access public
   */
  public function __construct( ) {
	parent::__construct();
  } // end of member function __construct

  /**
   * 
   *
   * @param mixed __name 

   * @return void
   * @access public
   */
  public function get_group( $__name ) {
  } // end of member function get_group

  /**
   * 
   *
   * @return void
   * @access public
   */
  public function get_trends( ) {
  } // end of member function get_trends

  /**
   * 
   *
   * @return void
   * @access public
   */
  public function getGroups($num) {
		parent::__construct();
  		$connection = parent::connect();
		$query = ("SELECT vdl_group.name, vdl_group.description, vdl_group.image, vdl_group.category, vdl_group.n_members, vdl_group.place_related, vdl_group.lat, vdl_group.long
					FROM `vdl_group`
					WHERE vdl_group.privacy_level LIKE 'open'
					LIMIT $num");
					
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				$temporal=array();
				$temporal["name"] = $row[0];
				$temporal["description"] = $row[1];
				$temporal["image"] = $row[2];
				$temporal["category"] = $row[3];
				$temporal["n_members"] = $row[4];
				$temporal["place_related"] = $row[5];
				$temporal["lat"] = $row[6];
				$temporal["long"] = $row[7];
				array_push($arresult,$temporal);
			}
			return $arresult;
		}
  } // end of member function get_groups

  
  	/**
	* Devuelve las últimas $num actualizaciones del usuario reconocido por su $token
	*
	* @param mixed _id_user 

	* @return array
	* @access public
	*/
	public function getLastGroups($token, $num){
		//Recuperamos el email del usuario que nos envía el token
		$TOKEN = new TOKEN();
		$email = $TOKEN->getEmailfromToken($token);
		
		
		//Recuperamos los ultimos grupos creados
		return $this->getGroups($num);
	}


  /**
   * 
   *
   * @return void
   * @access public
   */
  public function getUserGroups($idUser) {
  		$connection = parent::connect();
		$query = ("SELECT * 
					FROM  `vdl_u_belong` 
					INNER JOIN  `vdl_group` ON vdl_u_belong.group_name LIKE vdl_group.name
					WHERE vdl_u_belong.user_id LIKE $idUser");
					
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				$temporal=array();
				$temporal["user_id"] = $row[0];
				$temporal["is_admin"] = $row[2];
				$temporal["joined"] = TRUE;
				$temporal["name"] = $row[4];
				$temporal["description"] = $row[5];
				$temporal["image"] = $row[6];
				$temporal["category"] = $row[7];
				$temporal["privacy_level"] = $row[8];
				$temporal["n_members"] = $row[9];
				$temporal["place_related"] = $row[10];
				$temporal["lat"] = $row[11];
				$temporal["long"] = $row[12];
				array_push($arresult,$temporal);
			}
			return $arresult;
		}
  } // end of member function get_groups
  
  /**
   * 
   *
   * @return void
   * @access public
   */
  public function getNoUserGroups($idUser) {
  		$connection = parent::connect();
		$query = ("SELECT * 
					FROM  `vdl_group` 
					WHERE name NOT IN (SELECT `group_name`
										FROM `vdl_u_belong`
										WHERE user_id LIKE '$idUser')");
					
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				$temporal=array();
				$temporal["user_id"] = $idUser;
				$temporal["is_admin"] = null;
				$temporal["joined"] = FALSE;
				$temporal["name"] = $row[0];
				$temporal["description"] = $row[1];
				$temporal["image"] = $row[2];
				$temporal["category"] = $row[3];
				$temporal["privacy_level"] = $row[4];
				$temporal["n_members"] = $row[5];
				$temporal["place_related"] = $row[6];
				$temporal["lat"] = $row[7];
				$temporal["long"] = $row[8];
				array_push($arresult,$temporal);
			}
			return $arresult;
		}
  } // end of member function getNoUserGroups

  	/**
	* Devuelve las últimas $num actualizaciones del usuario reconocido por su $token
	*
	* @param mixed _id_user 

	* @return array
	* @access public
	*/
	public function getLastUserGroups($token){
		//Recuperamos el email del usuario que nos envía el token
		$TOKEN = new TOKEN();
		$idUser = $TOKEN->getEmailfromToken($token);
		
		
		//Recuperamos los ultimos grupos creados
		return $this->getUserGroups($idUser);
	}
	
	/**
	* Devuelve las últimas $num actualizaciones del usuario reconocido por su $token
	*
	* @param mixed _id_user 

	* @return array
	* @access public
	*/
	public function joinGroup($userid, $group){
		$connection = parent::connect();
		$query = ("INSERT INTO `vdl_u_belong`(`user_id`, `group_name`, `is_admin`, `date_joined`)
						VALUES ('$userid', '$group', '0', now())");
					
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	
	public function leaveGroup($userid, $group){
		$connection = parent::connect();
		$query = ("DELETE FROM `vdl_u_belong`
						WHERE (`user_id` LIKE '$userid' AND `group_name` LIKE '$group')");
					
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return $query;
		}
	}

} // end of GROUP
?>

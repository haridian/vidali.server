<?php
require_once 'CORE_MAIN.php';


/**
 * class NOTIFY
 * 
 */
class NOTIFY extends CORE_MAIN
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/

  /**
   * 
   * @access protected
   */
  protected $_id;

  /**
   * 
   * @access protected
   */
  protected $_id_creator;

  /**
   * 
   * @access protected
   */
  public $_id_receptor;

  /**
   * 
   * @access protected
   */
  protected $_type;

  /**
   * 
   * @access protected
   */
  protected $_checked;

  /**
   * 
   * @access protected
   */
  protected $_date;

  /**
   * 
   * @access protected
   */
  protected $_vdl_post_id;

  /**
   * 
   * @access protected
   */
  protected $_vdl_post_id_user;

  /**
   * 
   * @access protected
   */
  protected $_vdl_post_id_group;

  /**
   * 
   * @access protected
   */
  protected $_vdl_post_date_published;

	public function mark_read( $id_notification) {
  		$connection = parent::connect();
		$query = "UPDATE vdl_notify SET checked = 'viewed'
				WHERE id = $id_notification";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends


	public function notify_friend( $id_user1, $id_user2 ) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_notify(`id_creator`, `id_receptor`, `type`, `date`)
				VALUES ('$id_user1','$id_user2','friend', getdate())";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_friends

	public function notify_reply( $id_user1, $id_user2, $post_id, $date ) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_notify(`id_creator`, `id_receptor`, `type`, `date`, `post_id`)
				VALUES ('$id_user1','$id_user2','reply', '$date', '$post_id')";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_reply
	
	public function notify_message( $id_user1, $id_user2, $chat_id, $date ) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_notify(`id_creator`, `id_receptor`, `type`, `date`, `chat_id`)
				VALUES ('$id_user1','$id_user2','message', '$date', '$chat_id')";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function notify_message

		public function getUserNotify( $id_user) {
  		$connection = parent::connect();
		$query = "SELECT A.id,A.id_receptor,
						 A.id_creator, 
					 	 A.date,
					 	 A.type,
					 	 A.checked,
					 	 A.post_id,
					 	 A.chat_id,
					 	 B.nick,
					 	 B.image 
		 	 	 FROM vdl_notify A JOIN vdl_user B ON A.id_creator = B.id  
		 	 	 WHERE A.checked <> 'viewed' AND id_receptor = '$id_user'";
		$data=$connection->query($query);
		if(!$data){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			$arresult = array();
			while ($row = $data->fetch_assoc()) {
				array_push($arresult,$row);
			}
			return $arresult;
		}
	} // end of member function notify_message
	
} // end of NOTIFY
?>
<?php
require_once 'CORE_MAIN.php';


/**
 * class USER
 * 
 */
class USER extends CORE_MAIN
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/

  /**
   * 
   * @access protected
   */
  protected $_id;

  /**
   * 
   * @access protected
   */
  protected $_nickname;

  /**
   * 
   * @access public
   */
  public $_name;

  /**
   * 
   * @access protected
   */
  protected $_location;

  /**
   * 
   * @access protected
   */
  protected $_sex;

  /**
   * 
   * @access protected
   */
  protected $__bday;

  /**
   * 
   * @access protected
   */
  protected $_age;

  /**
   * 
   * @access protected
   */
  protected $_email;

  /**
   * 
   * @access protected
   */
  protected $_site;

  /**
   * 
   * @access protected
   */
  protected $_bio;

  /**
   * 
   * @access protected
   */
  protected $_avatar_id;

  /**
   * 
   * @access protected
   */
  protected $_prof_visits;

  /**
   * 
   * @access protected
   */
  protected $_prof_friends;

  /**
   * 
   * @access protected
   */
  protected $_prof_groups;

  /**
   * 
   * @access protected
   */
  protected $_session_id;

	
	/**
   * 
   *
   * @return void
   * @access public
   */
	public function __construct (){
		parent::__construct();
		$connection = parent::connect();
				return true;
		$args = func_get_args();
		$nargs = func_num_args();
		if ($nargs > 0){
			self::__construct1($args);
		}
	} // end of member function __construct


  /**
   * 
   *
   * @param string __USER 

   * @return void
   * @access public
   */
	public function load_user ($_USER){
		parent::__construct();
		$connection = parent::connect();
		//$user = htmlspecialchars(trim($_USER));
		$query = "SELECT
					vdl_user.id,
					vdl_user.email,
					vdl_user.nick,
					vdl_user.name,
					vdl_user.sex,
					vdl_user.birthdate,
					vdl_user.default_location,
					vdl_user.place,
					vdl_user.image,
					vdl_user.description,
					vdl_user.website,
					vdl_user.n_groups,
					vdl_user.n_contacts
				FROM vdl_user WHERE vdl_user.id=$_USER";
		$result= $connection->query($query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_assoc()){
				$this->_id = $row["id"];
				$this->_nickname = $row["nick"];
				$this->_name = $row["name"];
				$this->_location = $row["default_location"];
				$this->_sex = $row["sex"];
				$this->_bday = $row["birthdate"];
				$this->_bio = $row["description"];
				$this->_email = $row["email"];
				$this->_site = $row["website"];
				$this->_img_prof = $row["image"];
				$this->_prof_groups = $row["n_groups"];
				$this->_prof_friends = $row["n_contacts"];
				$this->_avatar_id = $row["image"];
			}
			return true;
		}
	} // end of member function __construct1

	
  /**
   * 
   *
   *  

   * @return void
   * @access public
   */
	public function firstLoad() {
  		$datos = array("nick" => $this->_nickname, "name" => $this->_name, "description" => $this->_bio, "location" => $this->_location, "avatar" => $this->_avatar_id);
  		return $datos;
	} // end of member function firstLoad
		
	
	
  /**
   * 
   *
   * @param mixed _id_user 

   * @return void
   * @access public
   */
	public function get_nick( $user ) {
		$connection = parent::connect();
		$query = ("SELECT vdl_user.nick
		 			FROM `vdl_user` 
		 			WHERE `email` LIKE '$user' OR `id` LIKE '$user'");
				   
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				array_push($arresult,$row[0]);
			}
			return $arresult[0];
		}
	} // end of member function get_nick

  /**
   * 
   *
   * @param mixed _id_user 

   * @return int
   * @access public
   */
  	public function get_id( $user ) {
	  	$connection = parent::connect();
		$query = ("SELECT vdl_user.id
		 			FROM `vdl_user` 
		 			WHERE `email` LIKE '$user' OR `nick` LIKE '$user'");
				   
		$result = $connection->query($query);
		$arresult=array();
		if (!$result) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message = $message . ' Whole query: ' . $query;
			die($message);
			return false;
		}
		else{
			while ($row = $result->fetch_array()) {
				array_push($arresult,$row[0]);
			}
			return $arresult[0];
		}
  	} // end of member function get_id
  
  /**
   * 
   *
   * @param mixed _id_user 

   * @return void
   * @access public
   */
  	public function get_friends( $_id_user ) {
  		$connection = parent::connect();
		$query = "SELECT vdl_user.nick
					FROM vdl_user
					WHERE vdl_user.id IN (SELECT vdl_friend_of.user2
					FROM vdl_friend_of
					WHERE vdl_friend_of.user1 LIKE '".$_id_user."' UNION SELECT vdl_friend_of.user1
																		FROM vdl_friend_of
																		WHERE vdl_friend_of.user2 LIKE '".$_id_user."')";
		$data=$connection->query($query);
		$arresult=array();
		if (!$data) {
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return false;
		}
		while ($row = $data->fetch_array()) {
			array_push($arresult,$row[0]);
		}
		return $arresult;
  } // end of member function get_friends


  /**
   * 
   *
   * @param mixed _id_user1 

   * @param mixed _id_user2 

   * @return void
   * @access public
   */
	public function add_friends( $id_user1,  $id_user2 ) {
  		$connection = parent::connect();
		$query = "INSERT INTO vdl_friend_of(`user1`, `user2`, `status`, `date_status`)
				VALUES ('$id_user1','$id_user2','unconfirmed', getdate())";
		$data=$connection->query($query);
		if($data != 1){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function set_friends
  
  /**
   * 
   *
   * @param mixed _id_user1 

   * @param mixed _id_user2 

   * @return void
   * @access public
   */
  	public function set_friends( $id_user1,  $id_user2, $status ) {
		$connection = parent::connect();
		$query = "UPDATE `vdl_friend_of`
					SET `status` = '$status', `date_status` = NOW()
					WHERE (`user1` LIKE '$id_user1' AND `user2` LIKE '$id_user2')
					OR (`user1` LIKE '$id_user2' AND `user2` LIKE '$id_user1')";
		$data=$connection->query($query);
		if(!$data){
			$message  = 'Invalid query: ' . $connection->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
	} // end of member function set_friends

  /**
   * 
   *
   * @param mixed _id_user1 

   * @return void
   * @access public
   */
  	public function delete_friend( $id_user1, $id_user2 ) {
  		$connection = parent::connect();
		$query = "DELETE FROM `vdl_friend_of`
					WHERE (`user1` LIKE '$id_user1' AND `user2` LIKE '$id_user2')
					OR (`user1` LIKE '$id_user2' AND `user2` LIKE '$id_user1')";
		$data=$connection->query($query);
		if(!$data){
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
			return FALSE;
		}
		else{
			return TRUE;
		}
  	} // end of member function delete_friend

  /**
   * 
   *
   * @return void
   * @access public
   */
  public function set_privacy( ) {
  } // end of member function set_privacy

  /**
   * 
   *
   * @return void
   * @access public
   */
  public function add_update( ) {
  } // end of member function add_update

  /**
   * 
   *
   * @return void
   * @access public
   */
  public function join_group( ) {
  } // end of member function join_group


  /**
   * 
   *
   * @return void
   * @access public
   */
  public function create( $email,$nick,$password,$name,$birthdate,$sex,
						$default_location,$place,$image,$description,$website,
						$privacy_level,$view_mode,$hide_mode, $mail_notify,
						$n_contacts,$n_groups ) {
  	$connection = parent::connect();
  	$pass = sha1(md5($password));
	$query = ("INSERT INTO `vdl_user`(`email`, `nick`, `password`, `name`, `birthdate`,
									`sex`, `default_location`, `place`, `image`, `description`, `website`,
									`privacy_level`, `view_mode`, `hide_mode`, `mail_notify`, `n_contacts`, `n_groups`)
				VALUES ('$email','$nick','$pass','$name','$birthdate','$sex',
						'$default_location','$place','$image','$description','$website',
						'$privacy_level','$view_mode','$hide_mode', '$mail_notify',
						'$n_contacts','$n_groups')");
			   
	$result = $connection->query($query);
	if($result != 1){
		$message  = 'Invalid query: ' . mysql_error() . "\n";
		$message .= 'Whole query: ' . $connection->error;
		die($message);
		return FALSE;
	}
	else{
		return TRUE;
	}
  } // end of member function create

  /**
   * 
   *
   * @return void
   * @access public
   */
  public function updateuser($id,$email,$nick,$name,$sex,$birthdate,
            $default_location,$place,$description,$website,
            $privacy_level) {
    $connection = parent::connect();
  $query = ("UPDATE `vdl_user` SET 
            `email`='$email', 
            `nick`='$nick', 
            `name`='$name', 
            `birthdate`='$birthdate',
            `sex`='$sex', 
            `default_location`='$default_location', 
            `place`='$place', 
            `description`='$description', 
            `website`='$website',
            `privacy_level`='$privacy_level'
            WHERE id=$id");
         
  $result = $connection->query($query);
  if(!$result){
    $message  = 'Invalid query: ' . $connection->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
    return FALSE;
  }
  else{
    return $query;
  }
  } // end of member function create


} // end of USER
?>

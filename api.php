<?php

include "vdl-include/class/CORE_MAIN.php";
foreach (glob("vdl-include/class/*.php") as $filename)
{
    include_once $filename;
}

require 'vdl-include/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

session_start();
ini_set('mssql.charset', 'UTF-8');

$app = new \Slim\Slim();

$app->get('/tokens', 'getTokens');
$app->get('/tokens/:id', 'getToken');
$app->post('/login/', 'doLogin');
$app->post('/lastUpdates', 'getLastUpdates');
$app->post('/user/add', 'setUser2');
//$app->post('/setUser', 'setUser');

$app->get('/recover/:email', 'recover');
$app->get('/update/friends/:id', 'getFriendUpdate');
$app->get('/getwall/:id', 'getUpdates');
$app->get('/getuserposts/:id', 'getUserPosts');
$app->post('/userpost/','addUpdate');
$app->delete('/userpost/:id','deletePost');
$app->get('/getgroups/', 'getALLGroups');
$app->get('/getallgroups/:id', 'getAllGroups');
$app->get('/getgroups/:id', 'getGroups');
$app->get('/user/:id', 'getUserData');
$app->post('/user/set/:id', 'setUserData');
$app->put('/friend/add/:id','addFriend');
$app->delete('/friend/delete/:id','deleteFriend');
$app->post('/user/update', 'setUserData');

$app->post('/setchat/:participantes', 'setChat');
$app->get('/chat/:id', 'getChats');
$app->post('/setmessage/:contenido', 'setMessage');
$app->get('/notification/:id', 'getNotifications');
$app->post('/groups/', 'joinGroup');
$app->delete('/groups/:id', 'leaveGroup');

$app->post('/contact','setFriend');

$app->run();
 
function getTokens() {
	//echo json_encode('token:1,token:2');
	//Recuperamos datos del usuario
		$USER = new USER("test@test.com");
		$datos = $USER->firstLoad();
		
		//Generamos token de sesion
		$TOKEN = new TOKEN();
		$token = $TOKEN->gen_token("1", "192.168.1.33", "no");
		
		//Añadimos el token al array asociativo que vamos a devolver
		$datos["token"] = $token;
		
		//$auth_session = array("session_auth" => "test");
		echo json_encode($datos);
}
 
function getToken($id) {
	if($id == 'vidaliapp'){
		$arr = array('vdl_utoken' => md5('token_example')."-".crypt("public_key"));
		echo json_encode($arr);
	}
}

function doLogin() {
	$LOGIN = new LOGIN;
	$sucess= $LOGIN->start($_POST['email'],$_POST['password']);
	if($sucess != false){
		//Recuperamos datos del usuario
		$USER = new USER();
		$USER->load_user($sucess["id"]); //cargar el user por el id y no por el email, en teoria $sucess->id
		$datos = $USER->firstLoad();
		//Generamos token de sesion
		$TOKEN = new TOKEN();
		$token = $TOKEN->gen_token($_POST['email'], $_POST['ip'], $_POST['remember'],$sucess["id"]); //recibir tambien el id del usuario como comprobacion
		//Añadimos el token al array asociativo que vamos a devolver
		$datos["token"] = $token;
		$datos["id"] = $sucess["id"];
		//$auth_session = array("session_auth" => "test");
		echo json_encode($datos);
	}
	else
		return false;
}

function getLastUpdates() {
	$UPDATE = new UPDATE();
		
	$datos = $UPDATE->getLastUpdates($_POST['token'], $_POST['num']);
	
	echo json_encode($datos);
}

function setUserData() {
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$USER = new USER();
	$sucess= $USER->updateuser($item->id_user,$item->email,$item->nick,$item->name,$item->sex,$item->birthdate,
							$item->default_location,$item->place,$item->description,$item->website,
							$item->privacy_level);
	if($sucess == true){		
		echo json_encode("{message: '$sucess'}");
	}
	else
		return "fallo insertado";
}


function setUser2() {
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$USER = new USER();
	$sucess= $USER->create($item->email,$item->nick,$item->password,$item->name,$item->sex,$item->birthdate,
							$item->default_location,$item->place,$item->image,$item->description,$item->website,
							$item->privacy_level,$item->view_mode,$item->hide_mode,$item->mail_notify,
							$item->n_contacts,$item->n_groups);
	if($sucess == true){		
		$LOGIN = new LOGIN;
		$loged= $LOGIN->start($item->email,$item->password);
		if($loged != false){
			//Recuperamos datos del usuario
			$USER = new USER();
			$USER->load_user($loged["id"]); //cargar el user por el id y no por el email, en teoria $sucess->id
			$datos = $USER->firstLoad();
			//Generamos token de sesion
			$TOKEN = new TOKEN();
			$token = $TOKEN->gen_token($item->email,$item->ip,$item->remember,$loged["id"]); //recibir tambien el id del usuario como comprobacion
			//Añadimos el token al array asociativo que vamos a devolver
			$datos["token"] = $token;
			$datos["id"] = $loged["id"];
			//$auth_session = array("session_auth" => "test");
			echo json_encode($datos);
		}
		else
			echo "fallo inicio";
	}
	else
		return "fallo insertado";
}

function addFriend($user1, $user2){
	$USER=new USER();
	$success = $USER->add_friends($user1, $user2);
	if($success){
		$NOTIFY = new NOTIFY();
		$NOTIFY->notify_friend($user1, $user2);
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function setFriend(){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$user2 = $item->id_user;
	$user1 = $item->id_creator;
	$notification = $item->id_notification;
	$status ="confirmed";
	$USER=new USER();
	$success = $USER->set_friends($user1, $user2, $status);
	if($success){
		$NOTIFY = new NOTIFY();
		$done = $NOTIFY->mark_read($notification);
		if($done){
			echo json_encode($success);
		}
	}
	else{
		return false;
	}
}

function deleteFriend($user1, $user2){
	$USER = new USER();
	$success = $USER->delete_friend($user1, $user2);
	if($success){
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function recover($email){
	$USER=new USER();
	$success = $USER->recover_password();
	if($success){
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function getFriendUpdate($id){
	
}

function getUpdates($id){
	$UPDATE = new UPDATE();
	$result = $UPDATE->getwallupdates($id);
	echo json_encode($result);
	return true;
}

function getUserPosts($id){
	$UPDATE = new UPDATE();
	$result = $UPDATE->getupdates($id,10);
	echo json_encode($result);
	return true;
}

function deletePost($post){
		
	$UPDATE = new UPDATE();
	
	$success = $UPDATE->deleteUserUpdate($post);
	
	if($success){
		echo json_encode($success);
	}
	else{
		return false;
	}
}

function getGroups($id){
	$GROUP = new GROUP();
		
	$datos = $GROUP->getUserGroups($id);
	
	//$datos = $GROUP->getLastUserGroups("e06dcd16c89cab196b4a974582a6b8130c8375b4", "10");
	
	echo json_encode($datos);
}

function getAllGroups($id){
	$GROUP = new GROUP();
		
	$datos = $GROUP->getUserGroups($id);
	$datos2 = $GROUP->getNoUserGroups($id);
	foreach ($datos2 as $dato) {
		array_push($datos, $dato);
	}
	
	//$datos = $GROUP->getLastUserGroups("e06dcd16c89cab196b4a974582a6b8130c8375b4", "10");
	
	echo json_encode($datos);
}

function joinGroup (){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$GROUP = new GROUP();
	$datos = $GROUP->joinGroup($item->userid, $item->group);
	echo json_encode($datos);
}

function leaveGroup ($id){
	$pos = strpos($id, '#', 0);
	$userid = substr($id, 0, $pos);
	$group = substr($id, $pos + 1, strlen($id));
	$group = str_replace('~', ' ', $group);
	$GROUP = new GROUP();
	$datos = $GROUP->leaveGroup($userid, $group);
	echo json_encode($datos);
}

function getUserData($id){
	
}

function getChats($id){
	$CHAT = new CHAT();
	
	$chats = $CHAT->get_chats($id);
	
	$MESSAGE = new MESSAGE();
	$mensajes = array();
	
	foreach ($chats as $elemento) {
		array_push($mensajes, array($elemento, $MESSAGE->get_messages($elemento)));
	}
	
	echo json_encode($mensajes);
}

function setChat($participantes){
	$CHAT = new CHAT();
		
	//$participantes= array(1, 2, 3, 4);
		
	$datos = $CHAT->create_chat();
	
	$valor = false;
	
	if ($datos >= 0) {
	
		$valor = true;
		
		foreach ($participantes as $elemento) {
			if ($valor == true)
				$valor = $CHAT->set_user_chat($datos, $elemento);
		}
		
	}
	
	echo json_encode($valor);	
}

function setMessage($id_conver,$id_user,$text){
	$MESSAGE = new MESSAGE();
	
	$valor = $MESSAGE->set_messages($id_conver, $id_user, $text);
	
	echo json_encode($valor);
}

function addUpdate(){
	$rawJSONString = file_get_contents('php://input');
	$item = json_decode($rawJSONString);
	$UPDATE = new UPDATE();
	$datos = $UPDATE->addUserUpdate($item->token, $item->text,$item->lat, $item->lon);
	echo json_encode($datos);
}

function getNotifications($id) {
	$NOTIFY = new NOTIFY();
	$datos = $NOTIFY->getUserNotify($id);	
	echo json_encode($datos);
}

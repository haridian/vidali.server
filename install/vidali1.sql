-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-12-2013 a las 16:00:34
-- Versión del servidor: 5.5.34-0ubuntu0.13.10.1
-- Versión de PHP: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `vidali1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_chat`
--

CREATE TABLE IF NOT EXISTS `vdl_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_msg_conver_vdl_conver1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_comment`
--

CREATE TABLE IF NOT EXISTS `vdl_comment` (
  `id_user` int(11) NOT NULL,
  `id_msg_ref` int(11) NOT NULL,
  `reply` varchar(140) COLLATE utf8_bin NOT NULL,
  `date_reply` datetime NOT NULL,
  PRIMARY KEY (`id_user`,`id_msg_ref`),
  KEY `fk_vdl_comment_vdl_msg1` (`id_msg_ref`),
  KEY `fk_vdl_comment_vdl_user1` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_config`
--

CREATE TABLE IF NOT EXISTS `vdl_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `config_value` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_event`
--

CREATE TABLE IF NOT EXISTS `vdl_event` (
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_msg` int(11) DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_bin NOT NULL,
  `category` set('university','business','party_spots','activism','public_places','restaurants','entertaiment','internet','television','family','fans','political','other') COLLATE utf8_bin DEFAULT NULL,
  `privacy_level` set('open','close','request') COLLATE utf8_bin DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `place_related` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `creator` int(11) NOT NULL,
  `lat` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `long` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `event_tittle_UNIQUE` (`description`),
  KEY `fk_vdl_event_vdl_msg1` (`id_msg`),
  KEY `fk_vdl_event_vdl_places1_idx` (`place_related`),
  KEY `fk_vdl_event_vdl_user1_idx` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_event`
--

INSERT INTO `vdl_event` (`name`, `id_msg`, `description`, `category`, `privacy_level`, `start_time`, `end_time`, `place_related`, `creator`, `lat`, `long`) VALUES
('FICULL', NULL, 'Fiesta para los nuevos alumnos de la ULL', 'university,party_spots', 'request', '2013-12-28 18:00:00', '2013-12-29 04:00:00', 'Universidad de La Laguna', 2, '16', '18'),
('Gana un bono de guagua Gratis!', NULL, 'Evento de un concurso de bonos de guagua.', 'business', 'open', '2013-12-25 00:00:00', '2013-12-26 00:00:00', 'Titsa - Empresa de transporte público', 1, '16', '18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_file`
--

CREATE TABLE IF NOT EXISTS `vdl_file` (
  `id` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_msg` int(11) DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `type` set('image','audio','video','other') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_file_vdl_msg1` (`id_msg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_friend_of`
--

CREATE TABLE IF NOT EXISTS `vdl_friend_of` (
  `user1` int(11) NOT NULL,
  `user2` int(11) NOT NULL,
  `status` set('unconfirmed','confirmed','blocked') COLLATE utf8_bin NOT NULL,
  `date_status` date NOT NULL,
  PRIMARY KEY (`user1`,`user2`),
  KEY `fk_vdl_friend_of_vdl_user1` (`user1`),
  KEY `fk_vdl_friend_of_vdl_user2` (`user2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_friend_of`
--

INSERT INTO `vdl_friend_of` (`user1`, `user2`, `status`, `date_status`) VALUES
(1, 2, 'unconfirmed', '2013-12-16'),
(1, 3, 'blocked', '2013-12-16'),
(2, 4, 'confirmed', '2013-12-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_group`
--

CREATE TABLE IF NOT EXISTS `vdl_group` (
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `description` varchar(250) COLLATE utf8_bin NOT NULL,
  `image` varchar(45) COLLATE utf8_bin NOT NULL,
  `category` set('university','business','party_spots','activism','public_places','restaurants','entertaiment','internet','television','family','fans','political','other') COLLATE utf8_bin NOT NULL,
  `privacy_level` set('open','close','request') COLLATE utf8_bin NOT NULL,
  `n_members` int(11) NOT NULL DEFAULT '0',
  `place_related` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `lat` varchar(45) COLLATE utf8_bin NOT NULL,
  `long` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`name`),
  KEY `fk_vdl_group_vdl_places1_idx` (`place_related`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_group`
--

INSERT INTO `vdl_group` (`name`, `description`, `image`, `category`, `privacy_level`, `n_members`, `place_related`, `lat`, `long`) VALUES
('Breakdance LL ', 'Grupo de bboys de La Laguna, nos solemos reunir en la zona principal del intercambiador de San Benito', '', 'entertaiment', 'open', 0, 'Titsa - Intercambiador San Benito', '16', '18'),
('Footing Santa Cruz', 'Asociación de personas que realizan footing en Santa Cruz, nuestro principal lugar de reunión es el parque la Granja', 'img_foot', 'entertaiment', 'request', 0, 'Parque La Granja', '16', '18'),
('Muevete en guagua!', 'Grupo de titsa. Promovemos el uso del transporte público a través de una campaña social totalmente adaptada a nuestro entorno.', 'img_muevete', 'business', 'open', 0, 'Titsa - Empresa de transporte público', '16', '18'),
('Paseantes La Granja', 'Grupo de personas a las que les gusta reunirse en el parque la granja.', 'img_granja', 'public_places', 'open', 0, 'Parque La Granja', '16', '18'),
('Universidad de La Laguna', 'Grupo de la comunidad universitaria', 'img_ull', 'university', 'open', 0, 'Universidad de La Laguna', '16', '18'),
('Vidali', 'Vidali User directory', 'img_vidali', 'internet', 'open', 0, 'Santa Cruz de Tenerife', '16', '18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_message`
--

CREATE TABLE IF NOT EXISTS `vdl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_send` datetime NOT NULL,
  `id_chat` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pm_msg` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`,`date_send`,`id_chat`),
  UNIQUE KEY `date_send_UNIQUE` (`date_send`),
  KEY `fk_vdl_msg_conver_vdl_user1_idx` (`user_id`),
  KEY `fk_vdl_msg_conver_vdl_conver1` (`id_chat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_notify`
--

CREATE TABLE IF NOT EXISTS `vdl_notify` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_creator` int(11) NOT NULL,
  `id_receptor` int(11) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `vdl_post_id` int(11) NOT NULL,
  `vdl_post_id_user` int(11) NOT NULL,
  `vdl_post_id_group` varchar(45) COLLATE utf8_bin NOT NULL,
  `vdl_post_date_published` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vdl_notify_vdl_user1_idx` (`id_creator`),
  KEY `fk_vdl_notify_vdl_user2_idx` (`id_receptor`),
  KEY `fk_vdl_notify_vdl_post1_idx` (`vdl_post_id`,`vdl_post_id_user`,`vdl_post_id_group`,`vdl_post_date_published`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_places`
--

CREATE TABLE IF NOT EXISTS `vdl_places` (
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `description` varchar(140) COLLATE utf8_bin NOT NULL,
  `type` set('public','business','other') COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `lat` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `long` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_places`
--

INSERT INTO `vdl_places` (`name`, `description`, `type`, `image`, `lat`, `long`) VALUES
('Parque La Granja', 'Parque La granja, SC de Tenerife', 'public', 'img_granja', '16', '18'),
('Santa Cruz de Tenerife', 'Ciudad de Santa Cruz de Tenerife', 'public', 'img_sc', '18', '16'),
('Titsa - Empresa de transporte público', 'Lugar donde se sitúa la entidad', 'business', 'img_titsa', '16', '18'),
('Titsa - Intercambiador San Benito', 'Intercambiado de San Benito para las paradas de Titsa', 'public', 'img_titsa_sb', '16', '18'),
('Universidad de La Laguna', 'Universidad de La Laguna', 'public', 'img_ull', '16', '18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_post`
--

CREATE TABLE IF NOT EXISTS `vdl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_group` varchar(45) COLLATE utf8_bin NOT NULL,
  `date_published` datetime NOT NULL,
  `text` varchar(140) COLLATE utf8_bin NOT NULL,
  `lat` varchar(30) COLLATE utf8_bin NOT NULL,
  `lon` varchar(30) COLLATE utf8_bin NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `place_related` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`,`id_user`,`id_group`,`date_published`),
  KEY `fk_vdl_msg_vdl_user1_idx` (`id_user`),
  KEY `fk_vdl_msg_vdl_group1_idx` (`id_group`),
  KEY `fk_vdl_post_vdl_places1_idx` (`place_related`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_token`
--

CREATE TABLE IF NOT EXISTS `vdl_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(45) COLLATE utf8_bin NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL,
  `expiration_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`user_id`),
  UNIQUE KEY `token` (`token`),
  KEY `fk_vdl_token_vdl_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `vdl_token`
--

INSERT INTO `vdl_token` (`id`, `user_id`, `token`, `ip`, `expiration_time`) VALUES
(3, 1, 'e06dcd16c89cab196b4a974582a6b8130c8375b4', '10.220.32.244, ', '2013-12-16 11:49:12'),
(4, 1, '40f1ac0455d78f5cbde2146d1c2debeea9f9492d', '10.220.32.244, ', '2013-12-16 11:49:59'),
(5, 1, '73b579e9eade9f5e6179c427d4d148148293d1d5', '10.220.32.244, ', '2013-12-16 12:08:10'),
(6, 1, '4276fa08dd82bee56f43f9f4e0df384793e72c46', '10.220.32.244, ', '2013-12-16 12:08:38'),
(7, 1, 'e3e61387e08b113f9813a355d632c689bc470060', '10.220.32.244, ', '2013-12-16 12:15:22'),
(8, 1, '64777382f7f64a3c45ab8585253861a261d13c08', '10.220.32.244, ', '2013-12-16 12:17:41'),
(9, 1, '78cfba6197c5f99a2d6e986cd740dadeac4d5cbe', '10.220.32.244, ', '2013-12-16 12:19:05'),
(10, 1, 'aedfa935c968109956a06f81e607b690e569ef80', '10.220.32.244, ', '2013-12-16 12:21:07'),
(11, 1, '44cf69e2edbb38a40dd8a6c2a67080eb010c04f2', '10.220.32.244, ', '2013-12-16 12:23:36'),
(12, 1, '18644a10bfb553fbda9a40321ae4fd53838ee07f', '10.220.32.244, ', '2013-12-16 12:37:36'),
(13, 1, '62359b3649e213f0409c20599c7253942da89401', '10.220.32.244, ', '2013-12-16 12:52:22'),
(14, 1, '7f076a13baa63cdac7467b9d21e9ef6734c02b73', '10.220.32.244, ', '2013-12-16 12:53:16'),
(15, 1, 'd94f77edc35f895d709e34a8094f5d2d8367675d', '10.220.32.244, ', '2013-12-16 13:23:57'),
(16, 1, '49b867de4fc4d886d31c0b2bb56d9ea1718d348c', '10.220.32.244, ', '2013-12-16 13:24:10'),
(17, 1, '75f220503423814ae434b44f2cdade09d100686a', '10.220.32.244, ', '2013-12-16 13:24:38'),
(18, 1, '466f2799d628510e63d95b372d3f742d81357e35', '46.222.127.104', NULL),
(19, 5, 'cb124be1878f94b1bb7d0c7985fd3bf4a85b8954', '46.222.127.104', '2013-12-17 14:33:23'),
(20, 1, 'ff3e63ca5568a892411627c2086d8a9abfdbe351', '46.222.127.104', '2013-12-17 16:41:35'),
(21, 1, '4b27675d764ade69891b9048c65f310110f1ff3b', '46.222.127.104', '2013-12-17 16:52:18'),
(22, 1, '528002fb2973f5904363b638f3b649511dcb0ca0', '46.222.127.104', '2013-12-17 16:56:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_tracking`
--

CREATE TABLE IF NOT EXISTS `vdl_tracking` (
  `user_id` int(11) NOT NULL,
  `lat` varchar(30) COLLATE utf8_bin NOT NULL,
  `lon` varchar(30) COLLATE utf8_bin NOT NULL,
  `time` datetime NOT NULL,
  `homespot_name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_trending`
--

CREATE TABLE IF NOT EXISTS `vdl_trending` (
  `topic` varchar(140) COLLATE utf8_bin NOT NULL,
  `count` int(10) NOT NULL,
  UNIQUE KEY `topic` (`topic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user`
--

CREATE TABLE IF NOT EXISTS `vdl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nick` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sex` enum('male','female') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `default_location` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `place` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(140) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `privacy_level` set('me','friends','public') COLLATE utf8_bin DEFAULT NULL,
  `view_mode` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `hide_mode` tinyint(1) DEFAULT NULL,
  `mail_notify` binary(1) DEFAULT NULL,
  `n_contacts` int(10) unsigned DEFAULT '0',
  `n_groups` int(10) unsigned DEFAULT '0',
  `session_key` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `session_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nick_UNIQUE` (`nick`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `vdl_user`
--

INSERT INTO `vdl_user` (`id`, `email`, `nick`, `password`, `name`, `sex`, `birthdate`, `default_location`, `place`, `image`, `description`, `website`, `privacy_level`, `view_mode`, `hide_mode`, `mail_notify`, `n_contacts`, `n_groups`, `session_key`, `session_id`) VALUES
(1, 'test@test.com', 'test', '4028a0e356acc947fcd2bfbf00cef11e128d484a', 'test', 'male', '1990-12-20', NULL, NULL, NULL, NULL, NULL, 'friends', NULL, NULL, NULL, 0, 0, NULL, NULL),
(2, 'iradiel@vidali.com', 'iradiel', '57eb06a8e1a610067d3f9764ecd0db60478a614d', 'Iradiel', 'male', '1990-10-31', NULL, NULL, 'imagen_ira', 'Un chico normal, en teoría.', NULL, 'public', NULL, NULL, NULL, 0, 0, NULL, NULL),
(3, 'jose@vidali.com', 'jose', '84ba2f41059c96db71f23ba27e47a236e57bc0c0', 'Jose Isaac', 'male', '1990-03-19', NULL, NULL, 'img_jose', 'Un chico alto, nacionalista canario.', NULL, 'friends', NULL, NULL, NULL, 0, 0, NULL, NULL),
(4, 'haridian@vidali.com', 'haridian', '323b5b1deac2c437274cc68bb1e2f5f11a13daa8', 'haridian', 'female', '1991-08-14', NULL, NULL, 'img_hari', NULL, NULL, 'public', NULL, NULL, NULL, 0, 0, NULL, NULL),
(5, 'cristo@vidali.com', 'cristo', '1716156b422ed00f8a6d3eb22c6da8c0ed1cb3fa', 'Cristopher Caamana', 'male', '1990-09-26', NULL, NULL, 'img_def', 'Vidali 1.0', NULL, 'public', NULL, NULL, NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_user_data`
--

CREATE TABLE IF NOT EXISTS `vdl_user_data` (
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(9) DEFAULT NULL,
  `mobile` int(9) DEFAULT NULL,
  `University` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Work` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Place` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group1` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group2` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group3` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group4` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `group5` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`email`),
  KEY `fk_vdl_user_opt_vdl_group1_idx` (`University`),
  KEY `fk_vdl_user_opt_vdl_group2_idx` (`Work`),
  KEY `fk_vdl_user_data_vdl_group1_idx` (`Place`),
  KEY `fk_vdl_user_data_vdl_group2_idx` (`group1`),
  KEY `fk_vdl_user_data_vdl_group3_idx` (`group2`),
  KEY `fk_vdl_user_data_vdl_group4_idx` (`group3`),
  KEY `fk_vdl_user_data_vdl_group5_idx` (`group4`),
  KEY `fk_vdl_user_data_vdl_group6_idx` (`group5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_user_data`
--

INSERT INTO `vdl_user_data` (`email`, `phone`, `mobile`, `University`, `Work`, `Place`, `group1`, `group2`, `group3`, `group4`, `group5`) VALUES
('cristo@vidali.com', NULL, 123456789, 'Universidad de La Laguna', 'Vidali', 'Paseantes La Granja', 'Breakdance LL ', 'Footing Santa Cruz', NULL, NULL, NULL),
('haridian@vidali.com', 987654321, NULL, 'Universidad de La Laguna', 'Muevete en guagua!', 'Breakdance LL ', 'Footing Santa Cruz', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_u_belong`
--

CREATE TABLE IF NOT EXISTS `vdl_u_belong` (
  `user_id` int(11) NOT NULL,
  `group_name` varchar(45) COLLATE utf8_bin NOT NULL,
  `is_admin` binary(1) NOT NULL,
  `date_joined` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`user_id`,`group_name`),
  KEY `fk_vdl_u_belong_vdl_user1` (`user_id`),
  KEY `fk_vdl_u_belong_vdl_group1` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vdl_u_belong`
--

INSERT INTO `vdl_u_belong` (`user_id`, `group_name`, `is_admin`, `date_joined`) VALUES
(1, 'Footing Santa Cruz', '1', '2013-12-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vdl_u_conver`
--

CREATE TABLE IF NOT EXISTS `vdl_u_conver` (
  `vdl_user_id` int(11) NOT NULL,
  `vdl_msg_conver_conver_ref` int(11) NOT NULL,
  PRIMARY KEY (`vdl_user_id`,`vdl_msg_conver_conver_ref`),
  KEY `fk_vdl_user_has_vdl_msg_conver_vdl_msg_conver1_idx` (`vdl_msg_conver_conver_ref`),
  KEY `fk_vdl_user_has_vdl_msg_conver_vdl_user1_idx` (`vdl_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `vdl_comment`
--
ALTER TABLE `vdl_comment`
  ADD CONSTRAINT `fk_vdl_comment_vdl_msg1` FOREIGN KEY (`id_msg_ref`) REFERENCES `vdl_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_comment_vdl_user1` FOREIGN KEY (`id_user`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_event`
--
ALTER TABLE `vdl_event`
  ADD CONSTRAINT `fk_vdl_event_vdl_msg1` FOREIGN KEY (`id_msg`) REFERENCES `vdl_post` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_event_vdl_places1` FOREIGN KEY (`place_related`) REFERENCES `vdl_places` (`name`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_event_vdl_user1` FOREIGN KEY (`creator`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_file`
--
ALTER TABLE `vdl_file`
  ADD CONSTRAINT `fk_vdl_file_vdl_msg1` FOREIGN KEY (`id_msg`) REFERENCES `vdl_post` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_friend_of`
--
ALTER TABLE `vdl_friend_of`
  ADD CONSTRAINT `fk_vdl_friend_of_vdl_user1` FOREIGN KEY (`user1`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_friend_of_vdl_user2` FOREIGN KEY (`user2`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_group`
--
ALTER TABLE `vdl_group`
  ADD CONSTRAINT `fk_vdl_group_vdl_places1` FOREIGN KEY (`place_related`) REFERENCES `vdl_places` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_message`
--
ALTER TABLE `vdl_message`
  ADD CONSTRAINT `fk_vdl_msg_conver_vdl_conver1` FOREIGN KEY (`id_chat`) REFERENCES `vdl_chat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_msg_conver_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_notify`
--
ALTER TABLE `vdl_notify`
  ADD CONSTRAINT `fk_vdl_notify_vdl_post1` FOREIGN KEY (`vdl_post_id`, `vdl_post_id_user`, `vdl_post_id_group`, `vdl_post_date_published`) REFERENCES `vdl_post` (`id`, `id_user`, `id_group`, `date_published`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_notify_vdl_user1` FOREIGN KEY (`id_creator`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_notify_vdl_user2` FOREIGN KEY (`id_receptor`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vdl_post`
--
ALTER TABLE `vdl_post`
  ADD CONSTRAINT `fk_vdl_msg_vdl_group1` FOREIGN KEY (`id_group`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_msg_vdl_user1` FOREIGN KEY (`id_user`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vdl_post_vdl_places1` FOREIGN KEY (`place_related`) REFERENCES `vdl_places` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_token`
--
ALTER TABLE `vdl_token`
  ADD CONSTRAINT `fk_vdl_token_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_tracking`
--
ALTER TABLE `vdl_tracking`
  ADD CONSTRAINT `fk_vdl_tracking_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_user_data`
--
ALTER TABLE `vdl_user_data`
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group1` FOREIGN KEY (`Place`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group2` FOREIGN KEY (`group1`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group3` FOREIGN KEY (`group2`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group4` FOREIGN KEY (`group3`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group5` FOREIGN KEY (`group4`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_data_vdl_group6` FOREIGN KEY (`group5`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_opt_vdl_group1` FOREIGN KEY (`University`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_opt_vdl_group2` FOREIGN KEY (`Work`) REFERENCES `vdl_group` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vdl_user_opt_vdl_user1` FOREIGN KEY (`email`) REFERENCES `vdl_user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_u_belong`
--
ALTER TABLE `vdl_u_belong`
  ADD CONSTRAINT `fk_vdl_u_belong_vdl_user1` FOREIGN KEY (`user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `vdl_u_belong_ibfk_1` FOREIGN KEY (`group_name`) REFERENCES `vdl_group` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vdl_u_conver`
--
ALTER TABLE `vdl_u_conver`
  ADD CONSTRAINT `fk_vdl_user_has_vdl_msg_conver_vdl_msg_conver1` FOREIGN KEY (`vdl_msg_conver_conver_ref`) REFERENCES `vdl_chat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vdl_u_conver_ibfk_1` FOREIGN KEY (`vdl_user_id`) REFERENCES `vdl_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
